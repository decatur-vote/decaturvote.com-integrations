<?php

namespace Dv\Integration;

class SearchIntegration implements \DecaturVote\Search\IntegrationInterface {
  
    protected \Lia $lia;   
  
    public function __construct(\Lia $lia){  
        $this->lia = $lia;  
    }  
  
    public function getPdo(): \PDO{  
        return $this->lia->pdo;  
    }  

    public function page_will_display(\Lia $lia): void {
        $lia->addResourceFile(__DIR__.'/../view/SearchPage.css');       
        $lia->addResourceFile(__DIR__.'/../view/SearchPage-Form.css');       
    }

    public function tag_feed_will_display(\Lia $lia): void {
        $lia->addResourceFile(__DIR__.'/../view/SearchPage.css');       
    }
    public function add_tag_will_display(\Lia $lia): void {
        $lia->addResourceFile(__DIR__.'/../view/Tags.css');
    }

    public function getHostWebsite(): string {
        return 'https://www.decaturvote.com';
    }

    public function getRssChannel(): array {
        /* @TODO make RSS channel dynamic, since I will have MANY different channels */
        return [
            'title'=>'Decatur Vote News',
            'link'=>'https://decaturvote.com/feed/', 
            'description'=>'Political and Election News for Decatur, Illinois',
        ];
    }

    public function can_create_tag(\Lia $lia, \DecaturVote\SearchDb\Tag $tag): bool{
        return $this->lia->user->has_role('admin');
    }
}
