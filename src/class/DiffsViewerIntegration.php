<?php

namespace Dv\Integration;

class DiffsViewerIntegration implements \DecaturVote\DiffsViewer\IntegrationInterface {
  
    protected \Lia $lia;   
    /**
     * a map of articles, used for caching to prevent multiple database round trips
     * @param $articles array<string uuid, array row> the sub-array is the result from the query for articles with the given uuid.
     */
    protected array $articles = [];
  
    public function __construct(\Lia $lia){  
        $this->lia = $lia;  
    }  
  
    public function getPdo(): \PDO{  
        return $this->lia->pdo;  
    }  
  
    public function canViewAdminPages(): bool {  
        return $this->lia->user->has_role('admin');
    }  
  
    public function get_current_text(string $uuid): string {  
        if (!$this->can_access_article($uuid))return '';
        $article = new \Dv\Story\Db\Article(['uuid_str' => $uuid]);
        // $article->uuid_str = $uuid;
        $text = $article->getHtmlBody();
        return $text;
    }  

    /**
     * Check if the article (and its diffs) can be accessed.
     * @param $uuid the article uuid
     * @return true if yes, false if no
     */
    protected function can_access_article(string $uuid): bool{
        if ($this->canViewAdminPages())return true;
        if (isset($this->articles[$uuid])){
            $rows = $this->articles[$uuid];
        } else {
            $rows = \ldb()->select('article', ['uuid_str'=>$uuid]);
            $this->articles[$uuid] = $rows;
        }
        if (count($rows)!=1)return false;
        if ($rows[0]['show_history'] == 0)return false;
        if ($rows[0]['status']=='public')return true;

        return false;  
    }
  
    public function can_access_diff(\DecaturVote\DiffDb\TextDiff $diff): bool {  
        return $this->can_access_article($diff->source_uuid);
    }  

    public function clean_diff_opps(string $diff_ops): string {
        return strip_tags($diff_ops);
    }

    public function clean_text(string $text): string{
        return $text;
    }

    /** add our styles */
    public function diffs_will_display(\Lia $lia): void {
        $lia->addResourceFile(__DIR__.'/../view/DiffsViewer.css');
    }
    
}
