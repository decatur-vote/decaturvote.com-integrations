<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# DecaturVote.com Integrations  
Integrations for DecaturVote.com are kept here. Please do not use our branding (colors or otherwise) or this repository directly. We share our integrations to give you an example of how to use our various open source packages. You should write your own integrations.  
  
## Available Integrations  
We might fail to document new integrations. They'll all be in [src/](/src/).  
- [taeluf/diffs-viewer](https://gitlab.com/decatur-vote/diffs-viewer)  
    - [DiffsViewer.css](/src/view/DiffsViewer.css)  
    - [DiffsViewerIntegration.php](/src/class/DiffsViewerIntegration.php)  
- [taeluf/web-search](https://gitlab.com/decatur-vote/web-search)  
    - [SearchPage.css](/src/view/SearchPage.css)  
    - [SearchIntegration.php](/src/class/SearchIntegration.php)  
  
## Licensing  
We do not grant a usage license to anyone for any of our integrations here. However, very little of what's in our integrations is truly proprietary, original, or copyrightable as far as we see it.  
  
So long story short, don't use these integrations to impersonate Decatur Vote or to rip off Decatur Vote's brand. Review these integrations for examples of how to setup your own software.   
  
No warranties & only you are liable for what you do with this software.   
  
*Note: You might see this is composer-installable. This is for internal convenience, not for your usage.*  
