<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/class/DiffsViewerIntegration.php  
  
# class Dv\Integration\DiffsViewerIntegration  
  
  
## Constants  
  
## Properties  
- `protected \Lia $lia;`   
- `protected array $articles = [];` a map of articles, used for caching to prevent multiple database round trips  
  
## Methods   
- `public function __construct(\Lia $lia)`   
- `public function getPdo(): \PDO`   
- `public function canViewAdminPages(): bool`   
- `public function get_current_text(string $uuid): string`   
- `protected function can_access_article(string $uuid): bool` Check if the article (and its diffs) can be accessed.  
- `public function can_access_diff(\DecaturVote\DiffDb\TextDiff $diff): bool`   
- `public function clean_diff_opps(string $diff_ops): string`   
- `public function clean_text(string $text): string`   
- `public function diffs_will_display(\Lia $lia): void` add our styles  
  
