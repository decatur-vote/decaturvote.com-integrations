<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/class/SearchIntegration.php  
  
# class Dv\Integration\SearchIntegration  
  
  
## Constants  
  
## Properties  
- `protected \Lia $lia;`   
  
## Methods   
- `public function __construct(\Lia $lia)`   
- `public function getPdo(): \PDO`   
- `public function page_will_display(\Lia $lia): void`   
  
